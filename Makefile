provision-local:
	sudo apt-get update
	sudo add-apt-repository -y ppa:ansible/ansible && sudo apt-get update
	sudo apt-get install -y ansible

provision-db:
	sudo ansible-playbook ./playbooks/start-containers.yml

clear-db:
	docker rm -f $(docker ps -a -q)

connect-dev-db:
	 psql postgresql://127.0.0.1:4000/catalog-db catalog-user