class ApplicationMailer < ActionMailer::Base
  default from: "mailer@rukkyrambles.com"
  layout 'mailer'
end
